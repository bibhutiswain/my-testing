package com.sample.demojpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
public class DemojpaApplication {

	@GetMapping(value="/test")
	public String testApp() {
		return "Test working";
	}
	
	public static void main(String[] args) {
		SpringApplication.run(DemojpaApplication.class, args);
	}
}
