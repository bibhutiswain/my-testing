package com.sample.demojpa.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "book_category")
public class BookCategory {

	private int id;
	private String name;
	private Set<Book> books;
	
	public BookCategory(String name, Set<Book> books) {
		super();
		this.name = name;
		this.books = books;
	}
	public BookCategory() {
		super();
	}
	
	public BookCategory(String name) {
		super();
		this.name = name;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@JsonBackReference
	@OneToMany(mappedBy = "bookCategory", cascade = CascadeType.ALL)
	public Set<Book> getBooks() {
		return books;
	}
	public void setBooks(Set<Book> books) {
		this.books = books;
	}
	
	
	
}
