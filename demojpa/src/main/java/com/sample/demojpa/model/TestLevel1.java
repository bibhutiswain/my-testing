package com.sample.demojpa.model;

public class TestLevel1 {
	
	private String field1;
	private String field2;
	private TestLevel2 test2;
	public TestLevel1() {
		super();
	}
	public TestLevel1(String field1, String field2, TestLevel2 test2) {
		super();
		this.field1 = field1;
		this.field2 = field2;
		this.test2 = test2;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public TestLevel2 getTest2() {
		return test2;
	}
	public void setTest2(TestLevel2 test2) {
		this.test2 = test2;
	}
	
	
	
}
