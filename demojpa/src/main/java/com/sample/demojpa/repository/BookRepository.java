package com.sample.demojpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sample.demojpa.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer>{
	public Book findById(int i);

}
