package com.sample.demojpa.service;

import java.util.List;

import com.sample.demojpa.model.Book;
import com.sample.demojpa.model.BookCategory;

public interface BookService {
	public String saveBookAndBookCatagory();
	
	public String updateBookRecord();
	public String updateBookCategory();
	
	public String deleteRecord();
	
	public Book getBookDetails(int bookId);
	
	public BookCategory getBookCatDetails(int catId);
}
