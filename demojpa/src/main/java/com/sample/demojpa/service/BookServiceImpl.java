package com.sample.demojpa.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sample.demojpa.model.Book;
import com.sample.demojpa.model.BookCategory;
import com.sample.demojpa.repository.BookCategoryRepository;
import com.sample.demojpa.repository.BookRepository;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	BookCategoryRepository bookCategoryRepository;
	
	@Autowired
	BookRepository bookrepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BookServiceImpl.class);
	
	@Override
	public String saveBookAndBookCatagory() {
		
		LOGGER.info("Inside saveBookAndBookCatagory");
		BookCategory bookCategoryA= new BookCategory("Catagory C");
		Set bookAs = new HashSet<Book>(){{
            add(new Book("Book A1", bookCategoryA));
            add(new Book("Book A2", bookCategoryA));
            add(new Book("Book A3", bookCategoryA));
        }};
		
        bookCategoryA.setBooks(bookAs);
        
		BookCategory bookCategoryB= new BookCategory("Catagory D");
		Set bookBs = new HashSet<Book>(){{
            add(new Book("Book B1", bookCategoryB));
            add(new Book("Book B2", bookCategoryB));
            add(new Book("Book B3", bookCategoryB));
        }};
		
        bookCategoryB.setBooks(bookBs);
        
        
        bookCategoryRepository.saveAll(new HashSet<BookCategory>() {{
            add(bookCategoryA);
            add(bookCategoryB);
        }});
        
        

		return "Saved";
	}

	@Override
	public String updateBookRecord() {

		Book bookDB=bookrepository.findById(2);
		bookDB.setName("New Name");
		Book savedBook=bookrepository.save(bookDB);
		return "Book saved:"+savedBook.getName();
	}

	@Override
	public String updateBookCategory() {
		
		BookCategory bookCategorydb = bookCategoryRepository.findById(1).get();
		
		Set<Book> newbooks = new HashSet<Book>(){{
	            				add(new Book("New Book", bookCategorydb));
	         				 }};
		bookCategorydb.setBooks(newbooks);
		
		return "Book saved :"+bookCategoryRepository.save(bookCategorydb);
	}

	
	@Override
	public String deleteRecord() {
		bookCategoryRepository.deleteById(1);
		return "Record deleted :1";
	}

	@Override
	public Book getBookDetails(int bookId) {

		LOGGER.info("Inside getBookDetails: "+bookId);
		
		return bookrepository.findById(bookId);
	}

	@Override
	public BookCategory getBookCatDetails(int catId) {

		return bookCategoryRepository.findById(catId).get();
	}
	

}
