package com.sample.demojpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.sample.demojpa.model.Book;
import com.sample.demojpa.model.BookCategory;
import com.sample.demojpa.repository.BookCategoryRepository;
import com.sample.demojpa.repository.BookRepository;
import com.sample.demojpa.service.BookServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class BookServiceImplTest {

	@Mock
	BookRepository bookrepository;
	
	
	@InjectMocks
	BookServiceImpl bookServiceImpl = new BookServiceImpl();
	
	
	@Before
	public void setUp() {
		BookCategory bookCategoryA= new BookCategory("Catagory A");
		Book book =new Book("Book Name", bookCategoryA);
		
		when(bookrepository.findById(2)).thenReturn(book);
		
		Book bookDB=bookrepository.findById(2);
		
		when(bookrepository.save(bookDB)).thenReturn(bookDB);
	}
	
	@Test
	public void updateBookRecordTest() {
		
		//assertTrue("Book saved:Book Name".equalsIgnoreCase(bookServiceImpl.updateBookRecord()));
		assertEquals("Book saved:New Name", bookServiceImpl.updateBookRecord());
		
	}
	
	
	
}
